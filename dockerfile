FROM alpine:latest
LABEL maintainer="Chris Weeks <chris@weeks.net.nz>" \
      project-site="https://github.com/dreibh/subnetcalc" \
      build-command="docker build -t subnetcalc ."
RUN apk update && \
    apk add --no-cache \
        alpine-sdk \
        autoconf \
        automake \
        curl \
        geoip \
        geoip-dev \
        libtool \
        m4 \
        unzip && \
    mkdir /build && \
    cd /build   && \
    curl -o ./subnetcalc.zip https://codeload.github.com/dreibh/subnetcalc/zip/master && \
    unzip ./subnetcalc.zip && \
    cd subnetcalc-master && \
    ./bootstrap && \
    ./configure --with-geoip && \
    make && \
    make install && \
    apk del \
        alpine-sdk \
        autoconf \
        automake \
        curl \
        geoip-dev \
        libtool \
        m4 \
        unzip && \
    apk add libstdc++
ENTRYPOINT ["/usr/local/bin/subnetcalc"]
