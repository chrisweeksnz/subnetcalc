# Overview

This is a container with subnetcalc. It's particularly useful on distributions that don't have it in their repos like Solus or CoreOS Container Linux.

The subnetcalc project can be found here:  https://github.com/dreibh/subnetcalc

This is merely packaging the authors excellent work.

### Usage

One-off use:

```shell
docker run -it --rm registry.gitlab.com/chrisweeksnz/subnetcalc:latest --help
```

Recommended method - add a function to your shell:

e.g. add the following to ~/.bashrc 
    
```shell
function subnetcalc() {
  docker run -it --rm registry.gitlab.com/chrisweeksnz/subnetcalc:latest $@
}
```

